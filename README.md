# formation-tunisie

Première proposition de déroulé possible pour J1 & 2. 

## Installations requises:

**A compléter en amont et à communiquer aux participant.e.s**

- R, RStudio
- sf, mapsf, rmapshapper, leaflet, readxl


## Format de présentations

notebook type présentation pour la première, notebook classiques


# Intro à la programmation, R & les notebooks (1h30) Ronan

## Pourquoi programmer ?

- Evite le clic-bouton, automatisation de tâches = gain de temps au final. 
- Gain en reproductibilité des procédures d'analyse
- Extensibilité des analyses (prendre un exemple en changeant une ligne de code et reproduire l'analyse sur un autre espace d'étude)
- Permet d'approfondir les méthodes (prendre l'exemple du code github derrière une fonction de MTA) > comment sont calculées des déviations, vraiment ?
- Capacités de calcul ++ : on peut faire des choses avec R Oou d'autres langages qui feraient sauter un SIG. 
- On a la sensation de progresser tout le temps: 1. ça marche ! 2. on optimise le code (boucles, fonctions) 3. On développe/adapte de nouvelles méthodes 4. On contribue à la communauté (librairies)

Mais...

- Un coût d'entrée certain. 
- Des programmes à suivre, qui peuvent périmer dans le temps (évolution de l'écosystème)


## Pourquoi R ?

- Bref historique du langage et de ses origines.
- Un seul environnement pour toutes les étapes de traitement ! 
- Gratuit, open source. 
- Un langage plutôt procédural qui convient bien aux protocoles d'analyse en SHS (étape 1, étape 2, etc.) > optimisation de chaines de traitement. 
-  Documentation riche et structurée (présentation des fonctions, aide) > permet au néophyte de bien s'approprier l'environnement. En profiter pour rappeler la distinction R Base / librairies. 
- Une large communauté, en évolution > librairies, aide (stackoverflow)
- L'ensemble de la chaîne de traitement de l'information géographique est couverte (figure d'Hugues).
- Peut traiter un grand nombre de formats de fichiers différents
- De nombreuses ouvertures professionnelles dans le champ de la science des données.
- Supportée par une IDE relativement facile à prendre en main: RStudio

Finir par une démonstration: lancer un script R dans RStudio, montrer les objets, les plots, l'aide, installer et lancer un package, etc. 
idée: import dataframe + geom, choisir un gouvernorat, lancer un plot de base.

## Notebooks

- Bref historique et intérêt (cf présentation Québec dans les grandes lignes)
- Les grandes famille de notebooks qui existent.
- Les différentes sorties (format, type de document)
- Le YAML
- Le langage Markdown
- Les chunks

Finir par lancer un quarto et le render dans RStudio avec le script précédent.




## Acquisition de données géographiques : lundi aprem (14h-16H) (Ronan)

**> partir du notebook créé dans le dépôt mais pas du tout aborder les aspects de représentation qui seront évoqués plus tard dans la semaine. On reste sur du plot de base.**

### Import de données à dimension géographique (tableaux de données + couche géographique) et manipulations de base autour des figures

- Import de données non spatiales: read.csv / read.xls > Importer un dataframe avec des codes géographiques.
- Quelques manipulations de base récurrentes: Créer de nouvelles variables / filtrer sur certaines lignes / créer une typologie avec ifelse etc.
- Transformer des champs avec des X/Y en couche de points.
- Importer une couche géographiques (geojson / shapefile / geopackage)
- Faire une jointure attributaire
- Afficher la table attributaire (initiation à sf > trouver la projection, la colonne géométries, etc.)
- Afficher les géométries (plot(x$geometry))
- Quelques plots de base (histogram, plot, barplot, boxplot)
- Marges du plot / Export de figures .pdf/.png

### API

- Présentation des grandes librairies interfaçant des API
- Import de données de la banque mondiale et de natural earth par ce biais
- Import de données depuis le package geodata (agrégateur de jeux de données spatiaux): places OSM, gouvernorats. 

### OSM

- Présentation des librairies de référence qui permettent la manipulation de données OSM: osmdata, nominatim, osmextract, maptiles, osrm.
- Exporter les hôpitaux, cliniques universités avec osmextract
- Petit exemple de géocodage avec nominatim ?
- calculer des temps de trajets par la route le gouvernorat de Sousse entre places OSM et cliniques et hôpitaux. 


## L'écosystème spatial de R : manipulation de vecteurs et de raster. Mardi 8h-10h 30

- L'écosystème spatial de R. Présentation générale, les packages de référence, etc. 

### Vecteur avec sf (Ronan)

- Anatomie d'un objet sf (rappel d'hier ?)
- Afficher les géométries et les attributs CRS (rappel d'hier ?)
- Changer de projection
- Opérations sur les géométries : agréger selon un champ, buffer, intersections... (cf [ça](https://github.com/rCarto/geomatique_avec_r/issues/6))
- Sélections et jointures spatiales
- Mesures (st_area, st_distance)
- Créer une grille régulière
- Simplifier les géométries avec rmapshapper.
 - conversion vecteur > raster
 - exporter un fichier
 - renvoit vers la doc de référence
 
### Raster avec terra (Brian-Malika)

- Présentation des spatraster

#### Manipulations de base
- import de données
- afficher un raster
- ajuster la résolution
- projection
- crop / mask
- conversion raster > vecteur

#### Algèbre spatial 

- opérations locales (Remplacement de valeurs, reclassification, NDVI)
- opérations focales (prise en compte des voisins, calcul de pente)
- opérations globales (valeurs moyennes, fréquence)
- opérations zonales (extraire des valeurs)
 - renvoit vers la doc de référence

> Qu'est-ce qu'on ne peut pas faire de façon optimale en R sur les opérations usuelles des géographes/sigistes ? digitalisation, autre ? 

## Cartographie avec R 

### Cartographie thématique avec mapsf (Nico)

- plot(x$geometry) / on est limités mais c'est pas mal quand même ! Fait la transition avec la session d'avant. 
- Afficher un fond de carte
- Carte en symboles proportionnels
- Carte choroplèthe + palettes de couleur + discrétisations
- Cartes de typo
- Stocks & ratios/typos
- Renvoit vers la documentation de référence

### Cartographies avancées (Nico)

- Cartes de discontinuité
- Cartogrammes

### Visualisations interactives avec leaflet (ou mapview) (Elina)

- WGS84 attendu
- Paramétrage du fond OSM (tuiles raster)
- Afficher une couche (jouer sur les couleurs typo / choro)
- Info-bulles

